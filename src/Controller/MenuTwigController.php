<?php

namespace Drupal\menu_twig\Controller;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Template\TwigEnvironment;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for menu twig operations.
 */
class MenuTwigController extends ControllerBase {

  /**
   * A TwigEnvironment instance.
   *
   * @var \Drupal\Core\Template\TwigEnvironment
   */
  private $twig;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  private $renderer;

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * MenuTwigController constructor.
   *
   * @param \Drupal\Core\Template\TwigEnvironment $twig
   *   A TwigEnvironment instance.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer service.
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list_module
   *   The module extension list.
   */
  public function __construct(TwigEnvironment $twig, Renderer $renderer, ModuleExtensionList $extension_list_module) {
    $this->twig = $twig;
    $this->renderer = $renderer;
    $this->moduleExtensionList = $extension_list_module;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('twig'),
      $container->get('renderer'),
      $container->get('extension.list.module'),
    );
  }

  /**
   * Get list of twig extensions/filters available.
   *
   * @param string $name
   *   The twig extension type ie. filter/function.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax modal response.
   */
  public function getTwigExtensions($name = 'filter') {
    $data = [];
    $options = [
      'dialogClass' => 'popup-dialog-class',
      'width' => '40%',
    ];
    $item_list = ($name == 'filter') ? $this->twig->getFilters() : $this->twig->getFunctions();
    foreach (array_keys($item_list) as $key => $item) {
      $data[] = [
        ['data' => ++$key],
        ['data' => $item, 'class' => 'command'],
        [
          'data' => new FormattableMarkup('<a class="menu-twig-command" href="#">@title</a>', ['@title' => $this->t('Copy')]),
        ],
      ];
    }
    $render = [
      '#theme' => 'table',
      '#header' => ['#ID', 'Name', $this->t('Copy Command')],
      '#rows' => $data,
      '#wrapper_attributes' => [
        'class' => [
          'wrapper__links__link',
        ],
      ],
    ];
    $response = new AjaxResponse();
    $response->addCommand(
      new OpenModalDialogCommand(
        $this->t('List of @names available with twig extensions', ['@name' => $name]),
        $this->renderer->render($render),
        $options
      )
    );
    return $response;
  }

  /**
   * Shows menu twig example.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax modal response.
   */
  public function getTwigExamples() {
    $options = [
      'dialogClass' => 'popup-dialog-class',
      'width' => '70%',
    ];
    $response = new AjaxResponse();
    $response->addCommand(new OpenModalDialogCommand(
        $this->t('Sample code and tricks to use the menu twig'),
        $this->twig->loadTemplate($this->moduleExtensionList->getPath('menu_twig') . '/templates/menu-twig-examples.html.twig')->render([]),
        $options)
    );
    return $response;
  }

}
